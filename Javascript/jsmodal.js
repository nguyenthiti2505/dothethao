// JAVASCRIPT FOR SLIDE
var slideModal = 1;
showSlidesModal(slideModal);
function plusSlides(n) {
    showSlidesModal(slideModal += n);
}
function currentSlide(n) {
    showSlidesModal(slideModal = n);
}
function showSlidesModal(n) {
    var i;
    var slidesModal = document.getElementsByClassName("cart-mySlides");
    var carts = document.getElementsByClassName("cart-dots");
    if (n > slidesModal.length) {slideModal = 1}    
    if (n < 1) {slideModal = slidesModal.length}
    for (i = 0; i < slidesModal.length; i++) {
        slidesModal[i].style.display = "none";  
    }
    for (i = 0; i < carts.length; i++) {
    carts[i].className = carts[i].className.replace(" active", "");
    }
    slidesModal[slideModal-1].style.display = "block";  
    carts[slideModal-1].className += " active";
}

$(document).on('click','.myBtn',function(){
  var myTargetModal = '#' + $(this).data('bid');
  $('#myModal-cart').hide();
  $('.cart-modal-content').hide();
  
  $('#myModal-cart').fadeIn();
  $(myTargetModal).fadeIn();
});
$("body" ).on( "click",".close", function() {
  $('#myModal-cart').hide();
  $('.cart-modal-content').hide();
});
