
$('.slideshow-container').slick({
  slidesToShow: 3,
  slidesToScroll: 5,
  autoplay: true,
  autoplaySpeed: 500,
  responsive: [
      {
          breakpoint: 600,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              vertical: true,
          }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          vertical:true
        }
      }
  ]
});
  // JS for slide home page
$('.MS-content-category').slick({
    slidesToShow: 5,
    slidesToScroll: 5,
    autoplay: true,
    autoplaySpeed: 500,
    responsive: [
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                vertical: true,
            }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            vertical:true
          }
        }
    ]
});
$('.MS-content-newproduct').slick({
  infinite: true,
  dots: true,
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: true,
  autoplaySpeed: 3000,
  responsive: [
      {
          breakpoint: 600,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              vertical: true,
          }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
  ]
});
$('.home-last-slider').slick({
    infinite: true,
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
        {
            breakpoint: 620,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                vertical: true,
            }
        },
    ]
});